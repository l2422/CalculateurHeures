describe("Calculateur d'heures", () => {

    // Marqueurs DOM
    const marqueurs = {
        "titre": 'titre',
        "tauxHoraire": 'taux-horaire',
        "heureDebut": 'heure-debut',
        "minuteDebut": 'minute-debut',
        "heureFin": 'heure-fin',
        "minuteFin": 'minute-fin',
        "dureeTextuelle": 'duree-textuelle',
        "dureeDecimaleTextuelle": 'duree-decimale-textuelle',
        "supprimerPlageHoraire": 'supprimer-plage-horaire',
        "ajouterPlageHoraire": 'ajouter-plage-horaire',
        "dureeDecimaleTotale": 'duree-decimale-totale',
        "totalEuros": 'total-euros',
    }

    beforeEach(() => {
        cy.visit('./index.html')
    })

    it('La page existe', () => {
        cy.marqueurCypress(marqueurs.titre)
            .should(($titre) => {
                expect($titre).to.contain("Calculateur d'heures")
            })
    })

    it('Par défaut les durées sont à zéro', () => {
        cy.marqueurCypress(marqueurs.dureeTextuelle, 1)
            .should(($dureeTextuelle) => {
                expect($dureeTextuelle).to.contain('00:00')
            })
        cy.marqueurCypress(marqueurs.dureeDecimaleTextuelle, 1)
            .should(($dureeDecimaleTextuelle) => {
                expect($dureeDecimaleTextuelle).to.contain('0.00')
            })
    })

    it('Un calcul simple fonctionne', () => {

        let donnees = {
            "saisies": {
                "tauxHoraire": 10,
                "horaires": [
                    {"debut":{"heure": "08","minute": "00"}, "fin":{"heure": "10","minute": "00"}}
                ]
            },
            "resultats": {
                "dureeDecimaleTotale": 2,
                "totalEuros": '20.00'
            }
        }

        cy.testerSaisies(marqueurs, donnees)
    })

    it('On peut ajouter une plage horaire', () => {
        cy.ajouterPlageHoraire(marqueurs.ajouterPlageHoraire)
    })

    it('Un calcul avec 2 plages fonctionne', () => {

        let donnees = {
            "saisies": {
                "tauxHoraire": 10,
                "horaires": [
                    {"debut":{"heure": "08","minute": "30"}, "fin":{"heure": "10","minute": "00"}},
                    {"debut":{"heure": "12","minute": "00"}, "fin":{"heure": "14","minute": "15"}},
                ]
            },
            "resultats": {
                "dureeDecimaleTotale": '3.75',
                "totalEuros": '37.50'
            }
        }

        cy.testerSaisies(marqueurs, donnees)
    })

    it('On peut supprimer des plages horaire', () => {
        cy.ajouterPlageHoraire(marqueurs.ajouterPlageHoraire)
        cy.ajouterPlageHoraire(marqueurs.ajouterPlageHoraire)
        cy.marqueurCypress(marqueurs.supprimerPlageHoraire, 1)
            .click()
        cy.marqueurCypress(marqueurs.supprimerPlageHoraire, 3)
            .click()
        cy.marqueurCypress(marqueurs.heureDebut, 1)
            .should('not.exist')
        cy.marqueurCypress(marqueurs.heureDebut, 2)
            .should('exist')
        cy.marqueurCypress(marqueurs.heureDebut, 3)
            .should('not.exist')
    })

})
