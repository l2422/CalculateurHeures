// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

// Permet de saisir des données de test et de vérifier les résultats attendus
Cypress.Commands.add('testerSaisies', (marqueurs, donnees) => {
    cy.marqueurCypress(marqueurs.tauxHoraire)
        .clear()
        .type(donnees.saisies.tauxHoraire)
    cy.remplirPlagesHoraire(marqueurs, donnees.saisies.horaires)
    cy.marqueurCypress(marqueurs.dureeDecimaleTotale)
        .should('have.text', donnees.resultats.dureeDecimaleTotale)
    cy.marqueurCypress(marqueurs.totalEuros)
        .should('have.text', donnees.resultats.totalEuros)
})

// Permet de remplir N plages horaire et d'en ajouter une si ce n'est pas la dernière plage horaire
Cypress.Commands.add('remplirPlagesHoraire', (marqueurs, horaires) => {
    for (var i=0;i<horaires.length;i++) {
        // Début
        cy.marqueurCypress(marqueurs.heureDebut, i+1)
            .select(horaires[i].debut.heure)
        cy.marqueurCypress(marqueurs.minuteDebut, i+1)
            .select(horaires[i].debut.minute)
        // Fin
        cy.marqueurCypress(marqueurs.heureFin, i+1)
            .select(horaires[i].fin.heure)
        cy.marqueurCypress(marqueurs.minuteFin, i+1)
            .select(horaires[i].fin.minute)
        if (horaires.length !== 1 && i+1 < horaires.length) {
            cy.ajouterPlageHoraire(marqueurs.ajouterPlageHoraire)
        }
    }
})

// Permet d'ajouter une plage horaire
Cypress.Commands.add('ajouterPlageHoraire', (marqueur) => {
    cy.marqueurCypress(marqueur)
        .click()
})

// Permet de récupérer un marqueur Cypress avec un index optionnel
Cypress.Commands.add('marqueurCypress', (marqueur, index = null) => {
    if (index !== null) {
        cy.get('[cy-name=' + marqueur + '-' + index + ']')
    } else {
        cy.get('[cy-name=' + marqueur + ']')
    }
})