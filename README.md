# Le calculateur d'heures de cours

Un outil simple pour remplir vos factures en fin de mois. Il est disponible en ligne gratuitement => [https://lfconsult.fr/calculateur_heures.html](https://lfconsult.fr/calculateur_heures.html)

![Le calculateur en action](/files/calculateur_heures.png "Le calculateur d'heures de cours")

Si vous avez des suggestions ou des bugs à faire remonter, n'hésitez pas à créer un ticket.

Le code est testé mais je ne pourrais être tenu responsable d'un bug. Il vous revient de vérifier la cohérence du résultat obtenu.

## Développeur

Pour démarrer le projet il suffit d'ouvrir le index.html à la racine du projet et d'éditer son code.

### Tests

```bash
yarn install
// Pour lancer la GUI
./node_modules/.bin/cypress open
// Ou lancer la CLI
./node_modules/.bin/cypress run
```

Si jamais Cypress ne s'exécute pas :

```bash
./node_modules/.bin/cypress.cmd install --force
```